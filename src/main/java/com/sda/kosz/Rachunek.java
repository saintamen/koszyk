package com.sda.kosz;

import java.util.ArrayList;
import java.util.List;

public class Rachunek {


    List<Produkt> listaZakupionychProduktow = new ArrayList<>();

    public void wypiszRachunek() {


        System.out.println("Lista zakupów:");
        for (Produkt nazwaProd : listaZakupionychProduktow) {
            System.out.println(nazwaProd.getNazwa());
        }

    }

    public double podsumujRachunekNetto() {

        double sumaNetto = 0;
        for (Produkt cenaNettoProd : listaZakupionychProduktow) {
            sumaNetto += cenaNettoProd.getCenaNetto();
        }
        System.out.println(sumaNetto);
        return sumaNetto;

    }

    public double podsumujRachunekBrutto() {

        double sumaBrutto = 0;
        for (Produkt cenaBruttoProd : listaZakupionychProduktow) {
            sumaBrutto += cenaBruttoProd.podajCeneBrutto();
        }
        System.out.println(sumaBrutto);
        return sumaBrutto;

    }

    public double zwrocWartoscPodatku() {
        System.out.println("Roznica podatku wynosi:");

        double roznica;

        roznica = podsumujRachunekBrutto() - podsumujRachunekNetto();

        System.out.println(roznica);
        return roznica;
    }

    // -------------------- gettery -------------------------

    public List<Produkt> getListaZakupionychProduktow() {
        return listaZakupionychProduktow;
    }

    @Override
    public String toString() {
        return "Rachunek{" +
                "listaZakupionychProduktow=" + listaZakupionychProduktow +
                '}';
    }

    public void dodajProdukty(List<Produkt> innaLista) {
        this.listaZakupionychProduktow.addAll(innaLista);
    }
}
