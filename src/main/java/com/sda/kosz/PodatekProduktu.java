package com.sda.kosz;

public enum PodatekProduktu {
    VAT8(8),
    VAT23(23),
    VAT5(5),
    NO_VAT(0);

    private double vati;

    PodatekProduktu(double vati) {
        this.vati = vati;
    }

    public double getVati(){
        return vati;
    }

}
