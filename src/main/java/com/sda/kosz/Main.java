package com.sda.kosz;

import java.awt.peer.ListPeer;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        Produkt produkt1 = new Produkt("Pilka", 52.5, PodatekProduktu.VAT23);
        Produkt produkt2 = new Produkt("Gra", 155.5, PodatekProduktu.VAT8);
        Produkt produkt3 = new Produkt("Kubek", 12.5, PodatekProduktu.VAT5);
        Produkt produkt4 = new Produkt("Zupa", 6.5, PodatekProduktu.NO_VAT);

        List<Produkt> listaZakupionychProduktow = new LinkedList<>();
        listaZakupionychProduktow.add(produkt1);
        listaZakupionychProduktow.add(produkt2);
        listaZakupionychProduktow.add(produkt3);
        listaZakupionychProduktow.add(produkt4);

        Rachunek rachKoncowy = new Rachunek();
        rachKoncowy.dodajProdukty(listaZakupionychProduktow);

        System.out.println(rachKoncowy);
        System.out.println("---------------------------------------");

        System.out.println("Wypisanie 1 :");
        rachKoncowy.wypiszRachunek();
        System.out.println("---------------------------------------");
        System.out.println("Wypisanie 2 :");
        rachKoncowy.wypiszRachunek();
        System.out.println("---------------------------------------");
        System.out.println("Wypisanie 3 :");
        rachKoncowy.wypiszRachunek();
    }
}
