package com.sda.kosz;

public class Produkt {
    private String nazwa;
    private double cenaNetto;
    PodatekProduktu iloscPodatku;

    public double podajCeneBrutto (){
        double cenaBrutto;
        return cenaBrutto = cenaNetto + (cenaNetto*iloscPodatku.getVati())/100;
    }

    // -------------------------- konstruktor -----------------------------

    public Produkt(String nazwa, double cenaNetto, PodatekProduktu iloscPodatku) {
        this.nazwa = nazwa;
        this.cenaNetto = cenaNetto;
        this.iloscPodatku = iloscPodatku;
    }


    // -------------------------- settery & gettery -----------------------


    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public double getCenaNetto() {
        return cenaNetto;
    }

    public void setCenaNetto(double cenaNetto) {
        this.cenaNetto = cenaNetto;
    }

    public PodatekProduktu getIloscPodatku() {
        return iloscPodatku;
    }

    public void setIloscPodatku(PodatekProduktu iloscPodatku) {
        this.iloscPodatku = iloscPodatku;
    }

    // ---------------------- to String ------------------------


    @Override
    public String toString() {
        return "Produkt: " + nazwa + ", posiada cene netto " + cenaNetto + " i podatek " + iloscPodatku + " procent";
    }
}
